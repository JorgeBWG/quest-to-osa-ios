
# OSA iOS #

La siguiente actividad muestra algunas de las habilidades del equipo de desarrollo iOS y ofrece la oprtunidad de medir las tuyas

## Que empiece la acción ##

Problema: Se necesita una App para iPhone que liste las peliculas de Star Wars que han sido estrenadas y/o anunciadas, mostrando su imagen y titulo. además si selecciono la celda se debe mostrar un pop up con la imagen de la película.

![](https://bytebucket.org/JorgeBWG/quest-to-osa-ios/raw/7ffb14e383b3598d407d222d5db3e8492b7ca256/giphy.gif)

## Instrucciones ##
1. Haz un fork a este repositorio.
2. Escribe tu cofigo en la rama 'develop'.
3. Cuando estes seguro de tus cambios y hayas finalizado tu prueba, haz un pull request a este repositorio.

## Debes.. ##

* Usar CollectionView.
* En orentación vertical deben caber 2 columnas exactas, sin importar el dispositivo.
* La imagen debe estar en escala según sus proporciones, tanto en el pop up como en las celdas.
* El popUp debe aparecer y desaparecer en solo una vista.
* El popUp debe desaparecer al apretar cualquier lugar fuera del popUp.
* La interfaz debe ser responsiva al tamaño de la pantalla.

### Consejos ###
* Usar la API de [OMDb](http://www.omdbapi.com/) es buena idea.
* Plantear tu propio diseño.
* Usar alguna libreria.
* Usar algún administrador de librerias

Ánimo y disfruta el desafio !!


